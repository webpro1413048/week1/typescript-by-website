interface Rectangle {
    height: number,
    width: number
  }
  
interface Color extends Rectangle{
    color:string
}

  const rectangle: Rectangle = {
    height: 20,
    width: 10
  };
  console.log(rectangle);

  const colorRec:Color={
    width:20,
    height:20,
    color:"red"
  }
  console.log(colorRec);