const names: string[] = [];
names.push("Dylan"); // no error
names.push("kittx"); // no error

console.log(names[0]);
console.log(names[1]);
console.log(names.length);
for (let i = 0; i < names.length; i++) {
    console.log(names[i]);
}
for (let ind in names) {
    console.log(names[ind]);
}
names.forEach(function(name){
    console.log(name);

}) 
  // names.push(3); // Error: Argument of type 'number' is not assignable to parameter of type 'string'.