const numbers = [1, 2, 3]; // inferred to type number[]
numbers.push(4); // no error
// comment line below out to see the successful assignment
let head: number = numbers[0]; // no error
numbers.forEach(function(value,index){
    console.log(value+" "+index)
})